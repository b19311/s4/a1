package com.zuitt.batch193;

public class Main {
    public static void main(String[] args) {
    User create = new User();

    User newUser = new User("Senando", "Lorenzo", 25, "Guiguinto, Bulacan");

    newUser.details();

    Course newCourse = new Course();
    newCourse.setName("Physics 101");
    newCourse.setDescription("Learn Physics");
    newCourse.setFee(2000.00);
    newCourse.setStartDate("June 12, 2022");
    newCourse.setEndDate("October 24, 2020");
    newCourse.setSeats(30);
    newCourse.setInstructorFirstName("Tee Jae");
    newCourse.setInstructorLastName("Natividad");

    newCourse.courseDetails();
    }
}
