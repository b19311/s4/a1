package com.zuitt.batch193;

public class Course {
    private String name;
    private String description;
    private int seats;
    private double fee;
    private String startDate;
    private String endDate;
    private String instructorFirstName;
    private String instructorLastName;


    public Course(){};

    public Course(String name, String description, int seats, double fee, String startDate, String endDate, String instructorFirstName, String instructorLastName){
        this.name = name;
        this.description = description;
        this.seats = seats;
        this.fee = fee;
        this.startDate = startDate;
        this.endDate = endDate;
        this.instructorFirstName = instructorFirstName;
        this.instructorLastName = instructorLastName;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public double getFee() {
        return fee;
    }

    public void setFee(double fee) {
        this.fee = fee;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getInstructorFirstName() {
        return instructorFirstName;
    }

    public void setInstructorFirstName(String instructorFirstName) {
        this.instructorFirstName = instructorFirstName;
    }

    public String getInstructorLastName() {
        return instructorLastName;
    }

    public void setInstructorLastName(String instructorLastName) {
        this.instructorLastName = instructorLastName;
    }

    public void courseDetails(){
        System.out.println("Course's name: ");
        System.out.println(getName());
        System.out.println("Course's Description");
        System.out.println(getDescription());
        System.out.println("Course's seats: ");
        System.out.println(getSeats());
        System.out.println("Course's instructor's first name: ");
        System.out.println(getInstructorFirstName());

    }
}
